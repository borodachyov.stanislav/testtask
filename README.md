# The task
Design and implement a web application with only API, which allows resizing images (PNG, JPEG, or any other format). As a user, I may use the service API to perform the transformation (for example, scale or resize the picture). 

You may simulate the picture transformation and just return the original one, supporting some formats is not needed also, the point of the task is not the resizing algorithm itself. Strive to demonstrate your ability to design the system as a whole and your skills by writing the production code. 

What is expected: 
* Design the system and define the requirements for the functionality.
* Design the HTTP/gRPC API.
* Implement the system, write the code, and run it. (you may use a Docker container to demonstrate the results, but it is not critical as well).

# Discussion
From first look, the task is very simple. It can be treated as single synchroneous request to some HTTP endpoint with request "Transform this image for me. I'm waiting for response". But! Even now, when we have gigabyte channels and teraflops systems, we still should keep in mind that resources are limited. And big file on highly loaded system can be scaled with 1000x factor... it can took some time and synchroneous approach looks weak in this case. And if something bad can happen - it will happen. With async approach user can submit request and receive some ticket, which can be used later to retrieve result of conversion. It's better to untie file upload, file processing and file download routines. And with this we see three entities:
* file upload (API). Here user should be able to submit file and request transformation procedure
* file conversion (Internal), where we can execute procedure itself
* file download (API), where we can retrieve result

![Sync request](./docs/imgs/Sync%20request.png)

![Async request](./docs/imgs/Async%20request.png)

## File upload
We have two main ways to upload files and commands for transformation:
* single shot
    Single shot submits file and metadata (conversion commands) at same time in single request. It can be treated as "Apply those commands to this file".
* two step A.
    With two step A procedure first step uploads file and retrieves temporary URL where you can submit commands. It's allows you to submit multiple commands for same image multiple times
* two step B.
    WIth two step B we're doing inversion and creating transformation "pipeline" which can accept any image.

@startuml

User -> TransformRequestAPI: create pipeline
TransformRequestAPI -> User: pipeline P created
User -> TransformRequestAPI: tranform FILE with pipeline P
TransformRequestAPI -> User: User: Accepted. Check later with ticket T

@enduml

Both two step procedures are good and more flexible than single shot. But for specified task it can be treated as overcomplicated. It raises questions like "What is the expiration time of image/pipeline?". We will take a look at single shot. Single shot can be unnatural for HTTP API since it requires us to upload FILE and textual list of commands at same time. But if we try to abstract from HTTP, it's nothing unnatural in mixage of text and binary data. Everything can be treated as stream of bytes with specified boundaries. I will use multipart/form-data content type as HTTP spec suggests. It will contain file and json file(text) with list of commands. It can be done by http client or browser and I don't see troubles here.

I will use HTTP requests since it's one of most easiest and widely spreaded approaches now. Moreover, since I'm familiar with Java and Spring, it will be easiest tool for me.

## File conversion
With file conversion we need to talk about several things like HA and durability. Since transformation operation can took some time, we need to:
* Store operation metadata until execution. We can have more incoming requests than we can process at this moment
* Provide durable state for them. If we have recevied request and faced with disaster - it's better to track state of operations and be able to rollback/repeat them in case of crash.
* Keep in mind horizontal scaling. It's better not to focus on single node since you always can reach ceil of single node performance. Distribution is better here.

To store metadata we can use journal based on any relational database with schema like

Table tasks
| taskId | fileURL | state | commandsList |
(where commandsList can be stored as string, array of strings or even json file)

With database we should cover durability and distribution cases due to ACID principle provided by databases. ACID principle will provide consistency of journal in case of disaster and transactional nature will allow us to organize proper concurrent access to it (e.g. approach like "SELECT FOR UPDATE" should cover our case).

![Database approach](docs/imgs/Database-based%20task%20distribution.png)

Other possible way is durable queues like kafka. With it we can organize queue between TransformRequestAPI and TransformationService. All TransformationService instances will take a look at same queue and queue system will take care about storage, durability and guarantees of single-receiver for each task.

![MessageBus approach](docs/imgs/MessageBus-based%20task%20distribution.png)

Both of those approaches has only one weak point: a file. It's usually a bad idea to store file in database (can be stored as BLOB but still have some troubles) or in a qeueue (usually has limit of single message). Files can be huge and can took megabytes. Moreover, in my case I'm adding limitation: 3rd-party tool is used for conversion. It's done in scope of other process. And tool can read input image only from filesystem. This is a somewhat rough restriction but I'm working with imagemagick package without embedding.

As we see, there is only one option for me - store file on disk. And it adds additional limitations on distribution nature. But things are not so worst as it imagined. There is VFS and distributed filesystems who can represent remote file as local. It can be done with NFS, SAMBA, IP2FS, HPFS, AmazonS3  and other distributed and not distributed filesystems. I will use NFS for simplification.

There is 3rd option to store journal: filesystem. Since we're looking at filesystem for files, we can also use it for journal. It's done with good old spooling folder where each task is represented as sub-folder with all necessary data. Instances can track content of the spool and can acquire locks based on filesystem rename/filecreation operations. I will use this approach since it requires less additional components and it looks a bit more hardcore for me. Yes, it requires to reinvent the whell and potentially adds tons of bugs in the code, but I'm treating this task as non-production playground. Anyway, It demonstrates same mechanism as used for datastore or queue systems.

## File download
Final result can be upload to any CDN system. Source system can reserve URL for resource before operation (in this case URL for content result can be passed on transformation request creation) or content can be proxied via source system (e.g. source system stores url after operation complete and redirect content retireval operations via 308 Moved Permanently response). I will use local filesystem content storage and predefined URLs.

![URL Reservation](docs/imgs/URL%20reservation.png)
![URL Redirect](docs/imgs/URL%20redirect.png)

# Design
## Overview
For simplification I'm looking at single-node solution. It should be easy-extendable to multi-node.
* Imagemagick tool is used to perform real conversion. 
* Spring boot is used as main framework. API is presented a set of REST requests.
* No databases or message buses are used for task distribution. All task operations are done with file
* System imelemented in single-node variant

## API
```
    Transformation endpoint:
        - POST /api/v1/transform
            Posts new transformation request. Body is multipart/form-request with two chunks: <file> for file content (application/binary) and <commands> with media type application/json containing list of commands.
            Response is 200 OK with task descriptor

        - GET /api/v1/transform/<taskId>
            Returns 200 OK with status of current task or 404 is task is not found

    Content endpoint:
        - GET /api/v1/content/<contentId>
            Returns 200 OK with requested content in body. Content will have Content-Type header with detected image type (or application/binary in case if image type is not detected)
```

### Format of commands operation
Operations are sent in JSON in wrapper object of format
```
{
    "commands": [cmd1, cmd2, ...]
}
```
There cmd is one of resize and scale operations. It's possible to post mutiple operations and they will be passed to convert tool, but result is not guaranteed.

```json
{
    "type": "scale",
    "factor": 2
}
```

```json
{
    "type": "resize",
    "width": 1000,
    "height": 500
}
```

### Format of task
```
{
    "taskId": <taskId>,
    "url": "url of content",
    "state": state of the task
}
```

## Tasks processing

### Task format
* Task is stored on disk in spool folder. Folder name is `taskId.taskState`.
* Folder contains `commands.json` file with list of received commands (same format as in API).
* Folder contains `source` file containing source image itself
* Folder contains `destination` file containing result of transformation

### Task processing
Currently there is file watching service who looks for spool folder and watches for folders with names `taskId.ready`. When detected, task is submitted to thread pool and performs execution.
If execution done with success - destination file is passed to ContentStorage service who stores image in other folder and providers access to it through Content API.

### Task processing (by threads)
[ThreadA]
* TransformationAPI receives request
* TransformationAPI passes request to TransformationService and receives taskId. Id is returned to caller.
* TransformationService saves content into `spool` folder.

[ThreadB]
* TransformationService watcher `taskId.ready` sub-folders events in `spool` folder.
* TransformationService prepares TransformationTask object and posts request to thread pool

[ThreadC]
* TransformationTask forks and executus Imagemagick `convert` utility
* TransformationTask checks sub-process code for 0
* TransformationTask forks and executus Imagemagick `identify` utility to check integrity of result and detect media type
* TransformationTask notifies TransformationService about task result
* TransformationService copies result to ContentStorage
* TransformationService cleanup task folder

### Components
![URL Redirect](docs/imgs/Components.png)

* TransformationAPI - REST endpoint for transformation requests
* ContentStorageAPI - REST endpoint for content fetch operations
* TransformationService - service who process incoming tasks in the task pool
* TransformationServiceWatcher - filesystem watcher who emits schedule operations for TransformationService
* TransformationTask - task who spawns imagemagick processes and do the work

### Distribution adaptation
System design works with local filesystem. It assumes all operations are done locally and all system components are spawned at same system.
But it can be adopted to distributed system with split of single app into three. With only one assumption: all systems have same filesystem like NFS (all nodes has same remote filesystem mounted)
* API - API component. Receives requests and stores task definition at filesystem
1. TransformationAPI
2. ContentStorageAPI

* Filewatcher daemon - component run on node with filesystem. Tracks file changes and kickoff trasnformation procedure on Trasnform Node
1. TransformationServiceWatcher

* Trasnform Node - component who performs transformation itself.
1. TransformationService
2. TransformationTask

![Filesystem approach](docs/imgs/Filesystem-based%20task%20distribution.png)