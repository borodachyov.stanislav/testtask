package org.stb.imgrsz.config;

import jakarta.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;

import java.nio.file.Path;
import java.util.Optional;

@ConfigurationProperties("dirs")
@Controller
@Slf4j
@Getter
@Setter
public class PathParameters {
    private static final Path DEFAULT_TASKCACHE_DIRECTORY = Path.of("./done-cache");
    private static final Path DEFAULT_SPOOL_DIRECTORY = Path.of("./spool");
    private static final Path DEFAULT_RESULTS_DIRECTORY = Path.of("./results");

    private Path taskCacheDirectory;
    private Path spoolDirectory;
    private Path resultDirectory;

    @PostConstruct
    public void init() {
        log.info("Reading configuration properties");

        taskCacheDirectory = Optional.ofNullable(taskCacheDirectory).orElse(DEFAULT_TASKCACHE_DIRECTORY);
        spoolDirectory = Optional.ofNullable(spoolDirectory).orElse(DEFAULT_SPOOL_DIRECTORY);
        resultDirectory = Optional.ofNullable(resultDirectory).orElse(DEFAULT_RESULTS_DIRECTORY);

        if (!taskCacheDirectory.toFile().exists()) {
            taskCacheDirectory.toFile().mkdirs();
        }

        if (!spoolDirectory.toFile().exists()) {
            spoolDirectory.toFile().mkdirs();
        }

        if (!resultDirectory.toFile().exists()) {
            resultDirectory.toFile().mkdirs();
        }

        log.info("Resolved task cache dir: {}", taskCacheDirectory);
        log.info("Resolved spool dir: {}", spoolDirectory);
        log.info("Resolved result dir: {}", resultDirectory);
    }
}
