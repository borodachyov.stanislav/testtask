package org.stb.imgrsz.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.stb.imgrsz.storage.StorageService;
import org.stb.imgrsz.storage.StorageServiceImpl;

@Configuration
public class TransformationConfig {

    @Bean
    public StorageService storageService(PathParameters parameters) {
        return new StorageServiceImpl(parameters.getResultDirectory());
    }

    @Bean(name = "transformationTaskPool")
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setPoolSize(5);
        threadPoolTaskScheduler.setThreadNamePrefix("Transformation");
        return threadPoolTaskScheduler;
    }
}
