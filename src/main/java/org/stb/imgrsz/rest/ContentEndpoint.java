package org.stb.imgrsz.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.stb.imgrsz.storage.StorageService;

@Controller
@Slf4j
public class ContentEndpoint {
    public static final String CONTENT_ID_PARAM = "contentId";
    public static final String BASE_PATH = Constants.API_PATH + "/content";
    public static final String TASK_PATH = BASE_PATH + "/{" + CONTENT_ID_PARAM + "}";

    @Autowired
    private StorageService storageService;

    @GetMapping(TASK_PATH)
    public ResponseEntity<Resource> getTaskResult(@PathVariable(CONTENT_ID_PARAM) String contentId) {
        FileSystemResource content = new FileSystemResource(storageService.getFilePath(contentId));

        ResponseEntity.BodyBuilder response = ResponseEntity.ok();
        response.contentType(storageService.getFileType(contentId));
        return response.body(content);
    }
}
