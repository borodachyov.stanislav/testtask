package org.stb.imgrsz.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.stb.imgrsz.rest.exceptions.BadSourceException;
import org.stb.imgrsz.rest.exceptions.DataProcessingException;
import org.stb.imgrsz.rest.exceptions.DuplicateContentException;
import org.stb.imgrsz.rest.exceptions.MissedContentDBRecordException;

@ControllerAdvice
public class ExceptionMapper {

    @ExceptionHandler
    public ResponseEntity<RestError> handleBadSource(BadSourceException exception) {
        RestError body = new RestError.RestErrorBuilder().description("incorrect input data: ").build();
        return ResponseEntity.badRequest().body(body);
    }

    @ExceptionHandler
    public ResponseEntity<RestError> handleDuplicateContent(DuplicateContentException exception) {
        RestError body = new RestError.RestErrorBuilder().description("Conflict content id: " + exception.getContentId()).build();
        return ResponseEntity.badRequest().body(body);
    }

    @ExceptionHandler
    public ResponseEntity<RestError> handleDataProcessing(DataProcessingException exception) {
        RestError body = new RestError.RestErrorBuilder().description("Unrecoverable error: " + exception.getMessage()).build();
        return ResponseEntity.internalServerError().body(body);
    }

    @ExceptionHandler
    public ResponseEntity<RestError> handleMissedContent(MissedContentDBRecordException exception) {
        RestError body = new RestError.RestErrorBuilder().description("Missed content ID: " + exception.getContentId()).build();
        return ResponseEntity.notFound().build();
    }
}
