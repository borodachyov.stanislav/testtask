package org.stb.imgrsz.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;
import org.stb.imgrsz.config.PathParameters;
import org.stb.imgrsz.rest.exceptions.BadSourceException;
import org.stb.imgrsz.rest.model.ImageTaskResponse;
import org.stb.imgrsz.transform.TaskDescriptor;
import org.stb.imgrsz.transform.TaskState;
import org.stb.imgrsz.transform.TransformationService;
import org.stb.imgrsz.transform.commands.TaskCommandsList;

import java.io.IOException;
import java.util.Map;

@RestController
public class TransformEndpoint {
    private static final String BASE_PATH = Constants.API_PATH + "/transform";
    private static final String ENTITY_PATH = BASE_PATH + "/{transformId}";

    @Autowired
    private PathParameters parameters;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private TransformationService service;

    @PostMapping(value = BASE_PATH, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ImageTaskResponse> submitTransformation(@RequestParam(name = "commands") MultipartFile commands, @RequestParam(name = "file") MultipartFile fileContent) {
        try {
            TaskCommandsList commandsList = jsonMapper.readValue(commands.getInputStream(), TaskCommandsList.class);
            String taskId = service.submitTask(commandsList.getCommands(), fileContent.getInputStream());

            ImageTaskResponse responseBody = new ImageTaskResponse();
            responseBody.setUrl(UriComponentsBuilder.fromPath(ContentEndpoint.TASK_PATH).build(Map.of(ContentEndpoint.CONTENT_ID_PARAM, taskId)).toString());
            responseBody.setId(taskId);
            responseBody.setState(TaskState.PLANING.getDescription());

            // Since we can't return immediate result - we're returning with 202 accepted since input data is valid but
            // it can't be processed immediately.
            return ResponseEntity.accepted().body(responseBody);
        } catch (IOException e) {
            // Can't save parameters to disk.
            throw new BadSourceException(e);
        }
    }

    @GetMapping(value = ENTITY_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ImageTaskResponse> getStatus(@PathVariable("transformId") String transformId) {
        TaskDescriptor status = service.getTaskStatus(transformId);
        if (status == null) {
            return ResponseEntity.notFound().build();
        } else {
            ImageTaskResponse responseBody = new ImageTaskResponse();
            responseBody.setUrl(UriComponentsBuilder.fromPath(ContentEndpoint.TASK_PATH).build(Map.of(ContentEndpoint.CONTENT_ID_PARAM, status.taskId())).toString());
            responseBody.setId(status.taskId());
            responseBody.setState(status.taskState().getDescription());

            return ResponseEntity.ok(responseBody);
        }
    }
}
