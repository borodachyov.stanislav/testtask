package org.stb.imgrsz.rest.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class DuplicateContentException extends ContentDBException {
    private final String contentId;
}
