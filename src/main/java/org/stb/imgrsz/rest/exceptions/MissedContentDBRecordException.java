package org.stb.imgrsz.rest.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class MissedContentDBRecordException extends ContentDBException {
    private final String contentId;
}
