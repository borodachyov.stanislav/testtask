package org.stb.imgrsz.rest.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.stb.imgrsz.transform.TaskState;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ImageTaskResponse {
    private String id;
    private String url;
    private String state;
}
