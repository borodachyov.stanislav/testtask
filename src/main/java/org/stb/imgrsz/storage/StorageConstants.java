package org.stb.imgrsz.storage;

public interface StorageConstants {
    String RESERVATION_FILE = "reservation";
    String EXPIRATION_FILE = "expiration";
    String CONTENT_FILE_NAME = "content";
    String MEDIATYPE_FILE_NAME = "mediatype";
}
