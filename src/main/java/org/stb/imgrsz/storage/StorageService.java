package org.stb.imgrsz.storage;

import org.springframework.http.MediaType;
import org.stb.imgrsz.rest.exceptions.CorruptedDBStructureException;
import org.stb.imgrsz.rest.exceptions.DataProcessingException;
import org.stb.imgrsz.rest.exceptions.DuplicateContentException;
import org.stb.imgrsz.rest.exceptions.MissedContentDBRecordException;

import java.io.InputStream;
import java.nio.file.Path;
import java.time.Duration;

public interface StorageService {
    /**
     * Stores new content for ID. Content is presented by path on filesystem
     * @param contentId - id of content to save
     * @param sourceContent - content body
     * @param mediaType - type of the stored content
     * @throws DuplicateContentException in case if id already presented in storage
     * @throws DataProcessingException in case if IO exception occurred on processing
     */
    void store(String contentId, Path sourceContent, MediaType mediaType);

    /**
     * Stores new content for ID. Content is presented by any stream
     * @param contentId - id of content to save
     * @param stream - content body
     * @param mediaType - type of the stored content
     * @throws DuplicateContentException in case if id already presented in storage
     * @throws DataProcessingException in case if IO exception occurred on processing
     */
    void store(String contentId, InputStream stream, MediaType mediaType);

    /**
     * Reserves place for content with ID. Later content can be stored with real data.
     * Reserved content throws exception on fetch but it differs from NOT found operation.
     * @param contentId - id of content for reservation
     * @param reservationPeriod - duration for reservation. Reservation will be freed if no content placed till end of reservation.
     */
    void reserve(String contentId, Duration reservationPeriod);

    /**
     * Set expiration flag for content
     * @param contentId - id of content for modification
     * @param period - period (since now) content is alive. Null value resets expiration
     * @throws MissedContentDBRecordException in case if content doesn't exist
     */
    void setContentExpiration(String contentId, Duration period);

    /**
     * Removes content from storage.
     * @param contentId - id of the content
     */
    void remove(String contentId);

    /**
     * Returns location of the content on filesystem
     * @param contentId - id of the content
     * @return location of the content file on filesystem
     */
    Path getFilePath(String contentId);

    /**
     * Returns media type for specified content id
     * @param contentId - id of the content
     * @return media type
     * @throws CorruptedDBStructureException in case of invalid content structure
     */
    MediaType getFileType(String contentId);
}
