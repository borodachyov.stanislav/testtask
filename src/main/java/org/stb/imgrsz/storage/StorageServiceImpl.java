package org.stb.imgrsz.storage;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.MediaType;
import org.springframework.util.StreamUtils;
import org.stb.imgrsz.rest.exceptions.*;
import org.stb.imgrsz.util.FileTreeRemoveVisitor;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * Provides some abstraction of file storage. Really it's just a filesystem storage.
 * But additionally it can store some meta information like content type.
 */
@Slf4j
@RequiredArgsConstructor
public class StorageServiceImpl implements StorageService {
    /**
     * Content root folder
     */
    private final Path contentFolder;

    @Override
    public void store(String contentId, Path sourceContent, MediaType mediaType) {
        checkDuplicateContent(contentId);

        try {
            // Store content
            getContentFolder(contentId).toFile().mkdirs();
            Files.copy(sourceContent, getContentFolder(contentId).resolve(StorageConstants.CONTENT_FILE_NAME));
        } catch (IOException e) {
            log.error("Can't store content for {}", contentId, e);
            throw new DataProcessingException(e);
        }

        // And media type
        writeMediaType(contentId, mediaType);
    }

    @Override
    public void store(String contentId, InputStream stream, MediaType mediaType) {
        checkDuplicateContent(contentId);

        getContentFolder(contentId).toFile().mkdirs();
        try (FileOutputStream outputStream = new FileOutputStream(getContentFolder(contentId).resolve(StorageConstants.CONTENT_FILE_NAME).toFile())) {
            StreamUtils.copy(stream, outputStream);
        } catch (IOException e) {
            log.error("Can't store content for {}", contentId, e);
            throw new DataProcessingException(e);
        }

        // And media type
        writeMediaType(contentId, mediaType);
    }

    @Override
    public void reserve(String contentId, Duration reservationPeriod) {
        checkDuplicateContent(contentId);

        Objects.requireNonNull(reservationPeriod);

        getContentFolder(contentId).toFile().mkdirs();
        final OffsetDateTime timestamp = OffsetDateTime.now().plus(reservationPeriod);
        final String timestampString = timestamp.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        writeFile(contentId, StorageConstants.RESERVATION_FILE, timestampString);
    }

    @Override
    public void setContentExpiration(String contentId, Duration period) {
        checkContentExists(contentId);

        if (period == null) {
            getContentFolder(contentId).resolve(StorageConstants.EXPIRATION_FILE).toFile().delete();
        } else {
            final OffsetDateTime timestamp = OffsetDateTime.now().plus(period);
            final String timestampString = timestamp.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
            writeFile(contentId, StorageConstants.EXPIRATION_FILE, timestampString);
        }
    }

    @Override
    public void remove(String contentId) {
        try {
            Files.walkFileTree(getContentFolder(contentId), new FileTreeRemoveVisitor());
        } catch (IOException e) {
            log.error("Can't remove content for {}", contentId, e);
            throw new DataProcessingException(e);
        }
    }

    // TODO: return URL?
    @Override
    public Path getFilePath(String contentId) {
        checkContentExists(contentId);
        checkContentReserved(contentId);
        checkContentExpiration(contentId);

        return getContentFolder(contentId).resolve(StorageConstants.CONTENT_FILE_NAME);
    }

    @Override
    public MediaType getFileType(String contentId) {
        checkContentExists(contentId);

        try {
            Path mediaType = getContentFolder(contentId).resolve(StorageConstants.MEDIATYPE_FILE_NAME);
            return MediaType.valueOf(Files.readString(mediaType));
        } catch (IOException | InvalidMediaTypeException e) {
            log.error("Can't read media type for {}", contentId, e);
            throw new CorruptedDBStructureException(e);
        }
    }

    /**
     * Returns path to folder where content is stored
     * @param contentId - id of the content
     * @return path of the folder where content should be stored
     */
    private Path getContentFolder(String contentId) {
        Path folder = contentFolder.resolve(contentId);

        // content path MUST be inside root path
        if (!folder.startsWith(contentFolder)) {
            throw new DataProcessingException("Invalid content id");
        }

        return folder;
    }

    /**
     * Validates if folder for content exists and throws exception otherwise
     * @param contentId - id of the content to check
     * @throws MissedContentDBRecordException in case of unknown content id
     */
    private void checkContentExists(String contentId) {
        Path contentDirPath = getContentFolder(contentId);
        if (!contentDirPath.toFile().exists()) {
            throw new MissedContentDBRecordException(contentId);
        }
    }

    private void checkDuplicateContent(String contentId) {
        Path contentDirPath = getContentFolder(contentId);
        if (contentDirPath.toFile().exists()) {
            throw new DuplicateContentException(contentId);
        }
    }

    private void checkContentExpiration(String contentId) {
        Path expirationFilePath = getContentFolder(contentId).resolve(StorageConstants.EXPIRATION_FILE);

        if (expirationFilePath.toFile().exists()) {
            try {
                OffsetDateTime expirationTimestamp = OffsetDateTime.parse(Files.readString(expirationFilePath), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
                if (expirationTimestamp.isBefore(OffsetDateTime.now())) {
                    log.debug("Remove expired content {}", contentId);
                    remove(contentId);

                    throw new MissedContentDBRecordException(contentId);
                }
            } catch (IOException e) {
                throw new DataProcessingException("Can't read expiration settings");
            }
        }
    }

    private void checkContentReserved(String contentId) {
        Path contentFolder = getContentFolder(contentId);

        // Check if don't have content
        if (contentFolder.resolve(StorageConstants.CONTENT_FILE_NAME).toFile().exists()) {
            return;
        }

        Path reservationFile = contentFolder.resolve(StorageConstants.RESERVATION_FILE);

        if (reservationFile.toFile().exists()) {
            try {
                OffsetDateTime expirationTimestamp = OffsetDateTime.parse(Files.readString(contentFolder.resolve(StorageConstants.RESERVATION_FILE)), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
                if (expirationTimestamp.isBefore(OffsetDateTime.now())) {
                    log.debug("Remove expired content {}", contentId);
                    remove(contentId);

                    throw new MissedContentDBRecordException(contentId);
                } else {
                    throw new ContentReservedException(contentId);
                }
            } catch (IOException e) {
                throw new DataProcessingException("Can't read expiration settings");
            }
        } else {
            remove(contentId);

            throw new MissedContentDBRecordException(contentId);
        }
    }

    private void writeMediaType(String contentId, MediaType mediaType) {
        writeFile(contentId, StorageConstants.MEDIATYPE_FILE_NAME, mediaType.toString());
    }

    private void writeFile(String contentId, String filename, String content) {
        try (OutputStream stream = new FileOutputStream(getContentFolder(contentId).resolve(filename).toFile())) {
            StreamUtils.copy(content, StandardCharsets.UTF_8, stream);
        } catch (IOException e) {
            log.error("Can't write content for {}:{}", contentId, filename, e);
            throw new DataProcessingException(e);
        }
    }
}
