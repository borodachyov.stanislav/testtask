package org.stb.imgrsz.transform;

public interface Constants {
    String COMMANDS_FILE = "commands.json";
    String SOURCE_FILE = "source";
    String TARGET_FILE = "target";
}
