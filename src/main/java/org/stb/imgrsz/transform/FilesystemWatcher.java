package org.stb.imgrsz.transform;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.stb.imgrsz.config.PathParameters;

import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * This service tracks spool directory for ready tasks.
 * When ready task is detected - it scheduled for execution
 */
@Service
@Slf4j
public class FilesystemWatcher {

    @Autowired
    private PathParameters parameters;

    @Autowired
    private TaskUtil taskUtilService;

    @Autowired
    private TransformationService transformationService;

    private WatchService watchService;

    private java.nio.file.WatchKey watchKey;

    @PostConstruct
    public void init() {
        try {
            watchService = FileSystems.getDefault().newWatchService();
            watchKey = parameters.getSpoolDirectory().register(watchService, ENTRY_CREATE, ENTRY_MODIFY);
            watchKey.reset();
        } catch (IOException e) {
            log.error("Can't register FS watcher service");
            // TODO: throw fatal error
            throw new IllegalArgumentException(e);
        }

        transformationService.reconstructTaskState();
    }

    @PreDestroy
    public void destroy() {
        watchKey.cancel();
    }

    @Scheduled(fixedRateString  = "${timers.task-spool:1000}")
    public void pollSpool() {
        log.info("Watching for FS changes");
        WatchKey key = watchService.poll();
        if (key == null) {
            return;
        }

        try {
            key.pollEvents().forEach(event -> {
                log.trace("Processing FS event {}:{}", event.kind(), event.context());
                if (event.kind() == OVERFLOW) {
                    return;
                }
                WatchEvent<Path> ev = (WatchEvent<Path>) event;

                try {
                    TaskDescriptor taskDescriptor = taskUtilService.parseTaskState(ev.context());
                    log.debug("Looking at {}:{}", taskDescriptor.taskId(), taskDescriptor.taskState());

                    if (taskDescriptor.taskState() == TaskState.READY) {
                        log.info("Scheduling {} for execution", taskDescriptor);
                        transformationService.scheduleTask(taskDescriptor.taskId());
                    }
                } catch (Exception e) {
                    log.warn("Can't process event change for {}. Skip", ev, e);
                }
            });
        } finally {
            key.reset();
        }
    }
}
