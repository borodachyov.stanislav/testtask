package org.stb.imgrsz.transform;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.stb.imgrsz.config.PathParameters;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Optional;

/**
 * Audits task result cache and removes outdated records
 */
@Service
@Slf4j
public class TaskCacheAudit {

    @Autowired
    private PathParameters pathParameters;

    @Autowired
    @Value("${timers.task-cache-ttl:900000}")
    private long expirationTimestamp;

    @Scheduled(fixedRateString  = "${timers.task-cache-audit:900000}")
    public void audit() {
        log.trace("Perform task cache audit");
        FileFilter filter = file -> {
            Path path = file.toPath();
            try {
                BasicFileAttributes attributes = Files.readAttributes(path, BasicFileAttributes.class);

                if (attributes.creationTime().toInstant().isBefore(Instant.now().minus(Duration.of(expirationTimestamp, ChronoUnit.MILLIS)))) {
                    log.info("Delete cache record {}", path);
                    return true;
                }
            } catch (IOException e) {
                log.warn("Can't read file attribute for {}. Ignore", path);
            }
            return false;
        };

        File[] expiredFiles = Optional.ofNullable(pathParameters.getTaskCacheDirectory().toFile().listFiles(filter)).orElse(new File[0]);
        Arrays.stream(expiredFiles).forEach(File::delete);
    }
}
