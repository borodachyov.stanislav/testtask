package org.stb.imgrsz.transform;

/**
 * Record to store task metadata like id and current state
 */
public record TaskDescriptor(String taskId, TaskState taskState) {
}
