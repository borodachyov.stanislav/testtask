package org.stb.imgrsz.transform;

public interface TaskListener {
    void taskStateChange(TransformationTask task, TaskState state);
}
