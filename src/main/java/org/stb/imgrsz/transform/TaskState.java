package org.stb.imgrsz.transform;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@RequiredArgsConstructor
@Getter
public enum TaskState {
    /**
     * Task is not ready for processing - not all input data received
     */
    PLANING("planning"),

    /**
     * Task is ready for processing - all input data is here
     */
    READY("ready"),

    /**
     * Acquired by system and scheduled for execution
     */
    SCHEDULED("scheduled"),

    /**
     * Task is active right now
     */
    INPROGRESS("in-progress"),

    /**
     * Task processing is done - result computed
     */
    DONE("done"),

    /**
     * Task finished with error
     */
    ERROR("error");

    private final String description;

    public static TaskState fromString(String description) {
        // Yes, this is not the best solution, but it's much easier
        return Arrays.stream(values())
                .filter(v -> v.getDescription().equals(description))
                .findAny()
                .orElse(null);
    }
}
