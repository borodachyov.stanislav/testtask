package org.stb.imgrsz.transform;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import org.stb.imgrsz.config.PathParameters;
import org.stb.imgrsz.rest.exceptions.DataProcessingException;
import org.stb.imgrsz.transform.commands.TaskCommand;
import org.stb.imgrsz.transform.commands.TaskCommandsList;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Slf4j
@Service
public class TaskUtil {

    @Autowired
    private PathParameters parameters;

    @Autowired
    private ObjectMapper jsonMapper;

    /**
     * Attempts to change task state.
     * @param taskId - id of the task
     * @param currentState - current state of the task
     * @param newState - expected new state of the task
     * @return true if state change applied
     */
    public boolean changeTaskState(String taskId, TaskState currentState, TaskState newState) {
        Path spoolDir = parameters.getSpoolDirectory();
        Path oldStateDir = spoolDir.resolve(getTaskDir(taskId, currentState));
        Path newStateDir = spoolDir.resolve(getTaskDir(taskId, newState));
        return oldStateDir.toFile().renameTo(newStateDir.toFile());
    }

    /**
     * Creates directory for new task and populates it with initial data
     * @param taskId - id of new task
     * @param content - bytestream of image content
     * @param commands - list of commands to apply to image
     * @return true if content was created properly
     * @throws DataProcessingException in case of error
     */
    public boolean createTaskDir(String taskId, InputStream content, List<TaskCommand> commands) {
        String dirName = getTaskDir(taskId, TaskState.PLANING);

        Path taskDir = parameters.getSpoolDirectory().resolve(dirName);
        if (!taskDir.toFile().mkdirs()) {
            throw new DataProcessingException("Can't create temp dir for content");
        }

        File sourceFile = taskDir.resolve(Constants.SOURCE_FILE).toFile();
        try (FileOutputStream sourceFileStream = new FileOutputStream(sourceFile)) {
            StreamUtils.copy(content, sourceFileStream);
        } catch (IOException e) {
            throw new DataProcessingException("Can't write temp file for content", e);
        }

        try {
            TaskCommandsList commandsList = new TaskCommandsList();
            commandsList.setCommands(commands);
            jsonMapper.writeValue(taskDir.resolve(Constants.COMMANDS_FILE).toFile(), commandsList);
        } catch (IOException e) {
            throw new DataProcessingException("Can't write commands file for content", e);
        }

        return changeTaskState(taskId, TaskState.PLANING, TaskState.READY);
    }

    /**
     * Process task directory and extract task id and current state
     * @param path - location of task directory
     * @return descriptor of the task
     * @throws IllegalArgumentException in case of illegal input args
     */
    public TaskDescriptor parseTaskState(Path path) {
        String filename = path.toFile().getName();
        String[] parts = filename.split("\\.");

        if (parts.length != 2) {
            log.debug("Filter out {} since do not match task format", filename);
            throw new IllegalArgumentException("Filename do not follow pattern");
        }

        return new TaskDescriptor(parts[0], TaskState.fromString(parts[1]));
    }

    /**
     * Returns name of the folder for task
     * @param taskId - id of the task
     * @param state - expected state of the task
     * @return folder name
     */
    public String getTaskDir(String taskId, TaskState state) {
        return taskId + "." + state.getDescription();
    }

    /**
     * Returns path to task folder
     * @param taskId - id of the task
     * @param state - expected state of the task
     * @return task folder path
     * @see #getTaskDir(String, TaskState)
     */
    public Path getTaskPath(String taskId, TaskState state) {
        return parameters.getSpoolDirectory().resolve(getTaskDir(taskId, state));
    }

    /**
     * Lookups state of the task
     * @param taskId - id of the task to lookup
     * @return id of the task
     */
    public TaskDescriptor getStatus(String taskId) {
        Predicate<Path> fileFilter = file -> file.toFile().getName().startsWith(taskId + ".");

        try {
            Path result = Stream.concat(
                    Files.walk(parameters.getSpoolDirectory(), 1),
                    Files.walk(parameters.getTaskCacheDirectory(), 1)
            ).filter(fileFilter).findAny().orElse(null);

            if (result != null) {
                return parseTaskState(result);
            }
        } catch (IOException e) {
            log.warn("Can't detect status of the task");
        }
        return null;
    }

    public void storeTaskResult(String taskId, TaskState state) {
        try {
            parameters.getTaskCacheDirectory().resolve(getTaskDir(taskId, state)).toFile().createNewFile();
        } catch (IOException e) {
            log.warn("Can't create cache rescord for {}:{}", taskId, state);
        }
    }

}
