package org.stb.imgrsz.transform;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.scheduling.SchedulingTaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.stb.imgrsz.config.PathParameters;
import org.stb.imgrsz.storage.StorageService;
import org.stb.imgrsz.transform.commands.TaskCommand;
import org.stb.imgrsz.transform.commands.TaskCommandsList;
import org.stb.imgrsz.util.FileTreeRemoveVisitor;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.nio.file.StandardWatchEventKinds.*;

@Service
@Slf4j
public class TransformationService {
    private static final Path SOURCE_FILE_NAME = Path.of(Constants.SOURCE_FILE);
    private static final Path DESTINATION_FILE_NAME = Path.of(Constants.TARGET_FILE);

    @Autowired
    private PathParameters parameters;

    @Autowired
    private StorageService storageService;

    @Autowired
    private TaskUtil taskUtilService;

    @Autowired
    @Qualifier("transformationTaskPool")
    private SchedulingTaskExecutor taskExecutor;

    @Autowired
    private ObjectMapper jsonMapper;

    private final TaskListener listener = new TaskListenerImpl();

    /**
     * Posts task for execution in thread pool
     * @param taskId - id of the task to run
     */
    /* package */ void scheduleTask(String taskId) {
        if (!taskUtilService.changeTaskState(taskId, TaskState.READY, TaskState.SCHEDULED)) {
            // It means someone already starting execution of the task.
            log.debug("Can't acquire task {} execution. Ignore", taskId);
            return;
        }

        Path taskDirPath = taskUtilService.getTaskPath(taskId, TaskState.SCHEDULED);
        try {
            log.info("Submitting {} for execution", taskId);
            TaskCommandsList commandsList = jsonMapper.readValue(taskDirPath.resolve(Constants.COMMANDS_FILE).toFile(), TaskCommandsList.class);

            TransformationTask task = new TransformationTask(taskUtilService, taskId, SOURCE_FILE_NAME, DESTINATION_FILE_NAME, commandsList.getCommands(), listener);
            taskExecutor.submit(task);
        } catch (IOException e) {
            log.error("Can't submit task", e);
            cleanupDir(taskDirPath.toFile());
        }
    }

    /**
     * Recovers task who stuck in strange state (e.g. due to incorrect shutdown).
     * Task state will be reset and started from scratch.
     * @param taskId - id of the task
     * @param state - current state of the task
     */
    private void rescheduleTask(String taskId, TaskState state) {
        taskUtilService.changeTaskState(taskId, state, TaskState.READY);
        scheduleTask(taskId);
    }

    /**
     * Submits new task for execution
     * @param commands - list of commands to apply
     * @param fileStream - bytestream of image
     * @return id of new task
     */
    public String submitTask(List<TaskCommand> commands, InputStream fileStream) {
        final String taskId = UUID.randomUUID().toString();
        taskUtilService.createTaskDir(taskId, fileStream, commands);

        return taskId;
    }

    public TaskDescriptor getTaskStatus(String taskId) {
        return taskUtilService.getStatus(taskId);
    }

    /**
     * Loads tasks from spool directory and runs them
     */
    /* package */ void reconstructTaskState() {
        File spoolDir = parameters.getSpoolDirectory().toFile();

        File[] taskDirs = spoolDir.listFiles(File::isDirectory);
        if (taskDirs == null) {
            log.warn("Spool directory doesn't exist");
            return;
        }

        Arrays.asList(taskDirs).forEach(file -> {
            TaskDescriptor taskDescriptor = taskUtilService.parseTaskState(file.toPath());
            String taskId = taskDescriptor.taskId();

            Optional.ofNullable(taskDescriptor.taskState()).ifPresent(state -> {
                log.trace("Process restoration for {}", file);

                switch (state) {
                    case PLANING:
                        log.info("Cleanup {} since it wasn't properly prepared", file.getName());
                        cleanupDir(file);
                        break;
                    case READY:
                        log.info("Schedule {} since it was ready but not done", file.getName());
                        rescheduleTask(taskId, state);
                        break;
                    case SCHEDULED:
                    case INPROGRESS:
                        log.info("Schedule {} since it wasn't complete", file.getName());
                        rescheduleTask(taskId, state);
                        break;
                    case DONE:
                        // Yes. It can be just copied. But we don't store media type. It need to be re-calculated.
                        // So, I just run it again
                        log.info("Schedule {} since result is not published", file.getName());
                        rescheduleTask(taskId, state);
                        break;
                    case ERROR:
                        log.info("Cleanup {} since it was completed unsuccessfully but not cleaned up", file.getName());
                        cleanupDir(file);
                        break;
                }
            });
        });
    }

    /**
     * Cleanups directory of the task
     *
     * @param taskDirectory - directory with task content
     */
    private void cleanupDir(File taskDirectory) {
        try {
            Files.walkFileTree(taskDirectory.toPath(), new FileTreeRemoveVisitor());
        } catch (IOException e) {
            log.warn("Can't cleanup dir {} due to exception", taskDirectory, e);
        }
    }

    /**
     * Publishes result of task work to globally available folder
     *
     * @param taskId - id of the task with result
     */
    private void copyResult(String taskId, MediaType resolvedMediaType) {
        log.info("Publish result of task {} as {}", taskId, resolvedMediaType);
        storageService.store(taskId, taskUtilService.getTaskPath(taskId, TaskState.DONE).resolve(DESTINATION_FILE_NAME), resolvedMediaType);
    }

    private void storeErrorResult(String taskId) {
        // TODO: this is just a stub for real processing error
        storageService.store(taskId, new ByteArrayInputStream("Error occurred on entity processing".getBytes(StandardCharsets.UTF_8)), MediaType.TEXT_PLAIN);
    }

    private class TaskListenerImpl implements TaskListener {

        @Override
        public void taskStateChange(TransformationTask task, TaskState state) {
            final String taskId = task.getTaskId();
            log.info("Task {} switched to {}", taskId, state);

            switch (state) {
                case DONE:
                    try {
                        log.info("Task is done and result should be published");
                        copyResult(taskId, task.getFinalMediaType());
                        taskUtilService.storeTaskResult(taskId, state);
                    } finally {
                        cleanupDir(taskUtilService.getTaskPath(taskId, state).toFile());
                    }
                    break;
                case ERROR:
                    // cleanup folders
                    try {
                        log.info("Task is done with error and should be cleaned up");
                        taskUtilService.storeTaskResult(taskId, state);
                        storeErrorResult(taskId);
                    } finally {
                        cleanupDir(taskUtilService.getTaskPath(taskId, state).toFile());
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
