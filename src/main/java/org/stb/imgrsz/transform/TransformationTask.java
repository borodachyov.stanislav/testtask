package org.stb.imgrsz.transform;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.stb.imgrsz.transform.commands.ResizeCommand;
import org.stb.imgrsz.transform.commands.ScaleCommand;
import org.stb.imgrsz.transform.commands.TaskCommand;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@RequiredArgsConstructor
@Slf4j
public class TransformationTask implements Runnable {
    private static final String TRANSFORM_COMMAND = "convert";

    private static final Map<String, MediaType> predefinedTypes;

    static {
        // Only these three
        predefinedTypes = Map.of(
            "JPEG", MediaType.IMAGE_JPEG,
            "PNG", MediaType.IMAGE_PNG,
            "GIF", MediaType.IMAGE_GIF
        );
    }

    // TODO: replace with prototype scope bean
    /**
     * task utils
     */
    private final TaskUtil taskUtil;

    /**
     * Id of the task inside filesystem
     */
    @Getter
    private final String taskId;

    /**
     * Name of source file
     */
    private final Path sourceName;

    /**
     * Name of destination result
     */
    private final Path destinationFile;

    /**
     * List of commands to apply to image
     */
    private final List<TaskCommand> commands;

    /**
     * Listener of task state or null if not presented
     */
    private final TaskListener listener;

    /**
     * Media type after last conversion
     */
    @Getter
    private MediaType finalMediaType;

    /**
     * Current state of the task
     */
    private TaskState state = TaskState.SCHEDULED;

    @Override
    public void run() {
        try {
            changeState(TaskState.INPROGRESS);

            Path workdir = prepareWorkDir();
            Path tmpFile = Files.createTempFile(workdir, taskId, null);

            // Form transformation string
            List<String> commandsList = new ArrayList<>(32);
            commandsList.add(TRANSFORM_COMMAND);
            commands.stream().map(this::processCommand).forEach(commandsList::addAll);
            commandsList.add(sourceName.toString());
            commandsList.add(tmpFile.toFile().getName());

            // Run imagemagick operation
            log.info("[{}]: execute command: '{}'", taskId, String.join(" ", commandsList));
            Process process = Runtime.getRuntime().exec(commandsList.toArray(new String[0]), null, workdir.toFile());
            process.waitFor();
            if (process.exitValue() == 0) {
                // Put result to place specified by our caller
                File destination = workdir.resolve(destinationFile).toFile();
                boolean renameResult = tmpFile.toFile().renameTo(destination);
                if (renameResult) {
                    log.info("[{}]:Renamed {} to {}", taskId, tmpFile, destination);
                } else {
                    log.warn("[{}]:Can't rename {} to {}", taskId, tmpFile, destination);
                    changeState(TaskState.ERROR);
                    return;
                }

                // Now detect media type. It's optional
                finalMediaType = detectMediaType();
                log.info("[{}]: resolved final type as {}", taskId, finalMediaType);

                changeState(TaskState.DONE);
                return;
            }
        } catch (IOException e) {
            // Shutdown execution with failure
            log.error("[{}]: execution failed", taskId, e);
        } catch (InterruptedException e) {
            log.error("[{}]: execution of sub process interrupted", taskId, e);
        }

        changeState(TaskState.ERROR);
    }

    private Path prepareWorkDir() {
        return taskUtil.getTaskPath(taskId, state);
    }

    private List<String> processCommand(TaskCommand command) {
        if (command instanceof ResizeCommand resizeCommand) {
            return Arrays.asList("-resize", resizeCommand.getWidth() + "x" + resizeCommand.getHeight());
        } else if (command instanceof ScaleCommand scaleCommand) {
            return Arrays.asList("-scale", (scaleCommand.getFactor() * 100) + "%");
        } else {
            return Collections.emptyList();
        }
    }

    private void changeState(TaskState newState) {
        // Rename directory of the file
        taskUtil.changeTaskState(taskId, state, newState);
        state = newState;

        listener.taskStateChange(this, state);
    }

    /**
     * Detect media type of the final image. It can change due to transformation and we need to read it from image itself
     * @return MediaType if detected or default one as backup case
     * @throws IOException - in case of disk read error
     * @throws InterruptedException - if thread was interrupted in action
     */
    private MediaType detectMediaType() throws IOException, InterruptedException {
        Path workdir = prepareWorkDir();

        List<String> commandsList = new ArrayList<>(16);
        commandsList.add("identify");
        commandsList.add(destinationFile.toString());

        // We're running identify tool who will detect image type and print it to stdout.
        // We're capturing stdout (just one line) and parsing it.
        Process process = Runtime.getRuntime().exec(commandsList.toArray(new String[0]), null, workdir.toFile());
        process.waitFor();
        if (process.exitValue() != 0) {
            return MediaType.APPLICATION_OCTET_STREAM;
        }

        String output = process.inputReader().readLine();
        String[] parts = output.split(" ");

        if (parts.length < 2) {
            return MediaType.APPLICATION_OCTET_STREAM;
        }

        return resolveMediaType(parts[1]);
    }

    private static MediaType resolveMediaType(String imgMagickType) {
        return predefinedTypes.getOrDefault(imgMagickType, MediaType.APPLICATION_OCTET_STREAM);
    }
}
