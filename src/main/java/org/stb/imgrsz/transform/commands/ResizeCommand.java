package org.stb.imgrsz.transform.commands;

import lombok.Data;

/**
 * Command to resize image to new X*Y dimensions
 */
@Data
public class ResizeCommand extends TaskCommand {
    /**
     * New image width
     */
    private int width;

    /**
     * New image height
     */
    private int height;
}
