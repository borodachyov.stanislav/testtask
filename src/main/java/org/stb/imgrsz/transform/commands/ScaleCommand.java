package org.stb.imgrsz.transform.commands;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Command for image scaling
 */
@Data
public class ScaleCommand extends TaskCommand {
    /**
     * Factor of image scale.
     * <ul>
     *     <li>&lt;1 - scale down</li>
     *     <li>=1 - noop transformation</li>
     *     <li>&gt;1 - scale up</li>
     * </ul>
     */
    @JsonProperty("factor")
    private Integer factor;
}
