package org.stb.imgrsz.transform.commands;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ResizeCommand.class, name = "resize"),
        @JsonSubTypes.Type(value = ScaleCommand.class, name = "scale")
})
public class TaskCommand {
}
