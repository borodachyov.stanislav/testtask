package org.stb.imgrsz.transform.commands;

import lombok.Data;

import java.util.List;

@Data
public class TaskCommandsList {
    private List<TaskCommand> commands;
}
